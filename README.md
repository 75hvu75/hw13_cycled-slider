## Теоретичні питання:

1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval()`.
2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
***
### Відповідь 1.

Головна різниця між функціями `setTimeout()` і `setInterval()` полягає в тому, що `setTimeout()` запускає функцію один раз, а `setInterval()` - з певним інтервалом від моменту її запуску до моменту її зупинки.
***
### Відповідь 2.

Якщо передати нульову затримку в функцію `setTimeout()`, функція виконається миттєво після того, як поточний стек виконання буде завершений.
***
### Відповідь 3.

Невикликання функції `clearInterval()` може призвести до небажаної поведінки програми. Це може призвести до витоку пам'яті і додаткового навантаження на браузер. 
***
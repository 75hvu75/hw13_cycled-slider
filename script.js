const images = document.querySelectorAll('.image-to-show');
const stopButton = document.getElementById('stop-button');
const resumeButton = document.getElementById('resume-button');

let currentIndex = 0;

function showCurrentImage() {
	images.forEach((image) => {
		image.style.display = 'none';
	});
	images[currentIndex].style.display = 'block';
	currentIndex = (currentIndex + 1) % images.length;
}

function startSlider() {
	showCurrentImage();
	intervalId = setInterval(showCurrentImage, 3000);
	resumeButton.disabled = true;
}

startSlider();

stopButton.addEventListener('click', function () {
	clearInterval(intervalId);
	resumeButton.disabled = false;
});

resumeButton.addEventListener('click', function () {
	startSlider();
});

